import React, { useState, useContext, useEffect } from 'react';
import ContactContext from '../../context/contact/ContactContext';

const ContactForm = () => {
    const contactContext = useContext(ContactContext);
    const { addContact, current, clearCurrent, updateContact } = contactContext;

    // Populate the form / clear form when current / contactContext changes
    useEffect(() => {
        if (current !== null) {
            setContact(current);
        } else {
            setContact({ name: '', email: '', phone: '', type: 'personal' });
        }
    }, [current, contactContext]);

    // contact state of the form
    const [contact, setContact] = useState({ name: '', email: '', phone: '', type: 'personal' });
    const { name, email, phone, type } = contact;

    // set state with form's value
    const onChange = (e) => setContact({ ...contact, [e.target.name]: e.target.value });

    // On submit button click
    const onSubmit = (e) => {
        e.preventDefault();
        if (current === null) {
            addContact(contact);
        } else {
            updateContact(contact);
        }
        clearAll();
    };

    // Clear value of current, clears out the form
    const clearAll = () => {
        clearCurrent();
    };
    return (
        <form onSubmit={onSubmit}>
            <h2 className="text-primary" style={{ float: 'left', marginBottom: '15px' }}>
                {current ? 'Edit contact' : 'Add contact'}
            </h2>
            <input
                type="text"
                placeholder="John Doe"
                name="name"
                value={name}
                onChange={onChange}
            />
            <input
                type="email"
                placeholder="john@email.com"
                name="email"
                value={email}
                onChange={onChange}
            />
            <input
                type="tel"
                placeholder="Your phone number"
                name="phone"
                value={phone}
                onChange={onChange}
            />
            <h5>Contact type</h5>
            <input
                type="radio"
                name="type"
                value="personal"
                id="personal"
                checked={type === 'personal'}
                onChange={onChange}
            />{' '}
            <label htmlFor="personal">Personal</label>{' '}
            <input
                type="radio"
                name="type"
                id="professional"
                value="professional"
                checked={type === 'professional'}
                onChange={onChange}
            />{' '}
            <label htmlFor="professional">Professional</label>
            <div className="">
                <input
                    type="submit"
                    value={current ? 'Update contact' : 'Add contact'}
                    className="btn btn-primary btn-block"
                />
            </div>
            {current && (
                <div>
                    <button className="btn btn-white btn-block" onClick={clearAll}>
                        Clear form
                    </button>
                </div>
            )}
        </form>
    );
};

export default ContactForm;
