import React, { useContext, useRef, useEffect } from 'react';
import ContactContext from '../../context/contact/ContactContext';

const ContactFilter = () => {
    const contactContext = useContext(ContactContext);
    const { filterContacts, clearFilters, filtered } = contactContext;

    const text = useRef('');

    useEffect(() => {
        if (filtered === null) {
            text.current.value = '';
        }
    });

    const onChange = (e) => {
        if (text.current.value !== '') {
            filterContacts(text.current.value);
        } else {
            clearFilters();
        }
    };

    return (
        <form>
            <input
                type="text"
                ref={text}
                placeholder="Filter contacts by name, email, number..."
                onChange={onChange}
            />
        </form>
    );
};

export default ContactFilter;
