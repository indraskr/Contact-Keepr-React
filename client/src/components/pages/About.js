import React from 'react';

const About = () => {
    return (
        <div className="about text-center my-3">
            <i className="far fa-id-card fa-4x"></i>
            <h1>Contact Keepr.</h1>
            <p>This is full stack React App for keeping contact in one place</p>
            <p className="bg-dark p my-2">
                <strong>Version: </strong>1.0.0
            </p>
        </div>
    );
};

export default About;
