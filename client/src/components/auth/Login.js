import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AlertContext from '../../context/alert/alertContext';
import AuthContext from '../../context/auth/authContext';

const Login = (props) => {
    const alertContext = useContext(AlertContext);
    const { setAlert } = alertContext;
    const authContext = useContext(AuthContext);
    const { login, error, isAuthenticated, clearErrors } = authContext;

    useEffect(() => {
        if (error !== null && (error.id === 'INVALID_PASSWORD' || error.id === 'INVALID_EMAIL')) {
            setAlert(error.msg, 'danger');
            clearErrors();
        }
        if (isAuthenticated) {
            props.history.push('/');
        }
        // eslint-disable-next-line
    }, [error, isAuthenticated, props.history]);

    const [user, setUser] = useState({
        email: '',
        password: '',
    });

    const { email, password } = user;

    const onChange = (e) => setUser({ ...user, [e.target.name]: e.target.value });

    const onsubmit = (e) => {
        e.preventDefault();

        if (email === '' || password === '') {
            setAlert('Please fill out all required fields', 'danger');
        } else {
            login({ email, password });
        }
    };

    return (
        <div className="form-container">
            <h1>
                Account <span className="text-primary">Login</span>{' '}
            </h1>

            <form onSubmit={onsubmit}>
                <div className="form-group">
                    <label htmlFor="email">Email Address</label>
                    <input type="email" name="email" value={email} onChange={onChange} />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        name="password"
                        value={password}
                        onChange={onChange}
                        minLength="6"
                        required
                    />
                </div>

                <input type="submit" value="Login" className="btn btn-primary btn-block" />
            </form>

            <p className="text-center">
                New user?{' '}
                <Link to="/register">
                    <strong>Register here</strong>
                </Link>{' '}
            </p>
        </div>
    );
};

export default Login;
