import React, { useReducer } from 'react';
import ContactContext from './ContactContext';
import contactReducer from './contactReducer';
import axios from 'axios';
import {
    ADD_CONTACT,
    DELETE_CONTACT,
    SET_CURRENT,
    CLEAR_CURRENT,
    UPDATE_CONTACT,
    FILTER_CONTACT,
    CLEAR_FILTER,
    CONTACT_ERROR,
    GET_CONTACTS,
    CLEAR_CONTACTS,
} from '../types';

const ContactState = (props) => {
    const initialState = {
        contacts: null,
        // for contact edit
        current: null,
        filtered: null,
        error: null,
    };

    const [state, dispatch] = useReducer(contactReducer, initialState);

    // Get contact
    const getContacts = async () => {
        try {
            const res = await axios.get('/api/contacts');
            dispatch({ type: GET_CONTACTS, payload: res.data });
        } catch (err) {
            dispatch({ type: CONTACT_ERROR, payload: err.response.msg });
        }
    };

    // Add Contact
    const addContact = async (contact) => {
        const config = {
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const res = await axios.post('/api/contacts', contact, config);
            dispatch({ type: ADD_CONTACT, payload: res.data });
        } catch (err) {
            dispatch({ type: CONTACT_ERROR, payload: err.response.msg });
        }
    };

    // Update contact
    const updateContact = async (contact) => {
        const config = {
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const res = await axios.put(`/api/contacts/${contact._id}`, contact, config);
            dispatch({ type: UPDATE_CONTACT, payload: res.data });
        } catch (err) {
            dispatch({ type: CONTACT_ERROR, payload: err.response.msg });
        }
    };

    // Delete Contact
    const deleteContact = async (id) => {
        try {
            await axios.delete(`/api/contacts/${id}`);
            dispatch({ type: DELETE_CONTACT, payload: id });
        } catch (err) {
            dispatch({ type: CONTACT_ERROR, payload: err.response.msg });
        }
    };

    // Clear contacts from state
    const clearContacts = () => dispatch({ type: CLEAR_CONTACTS });

    // Set current contact to form
    const setCurrent = (contact) => dispatch({ type: SET_CURRENT, payload: contact });

    // Clear current contact from form
    const clearCurrent = () => dispatch({ type: CLEAR_CURRENT });

    // Filter contacts
    const filterContacts = (text) => dispatch({ type: FILTER_CONTACT, payload: text });

    // Clear filter
    const clearFilters = () => dispatch({ type: CLEAR_FILTER });

    return (
        <ContactContext.Provider
            value={{
                contacts: state.contacts,
                current: state.current,
                filtered: state.filtered,
                error: state.error,
                addContact,
                deleteContact,
                setCurrent,
                clearCurrent,
                updateContact,
                filterContacts,
                clearFilters,
                getContacts,
                clearContacts,
            }}
        >
            {props.children}
        </ContactContext.Provider>
    );
};

export default ContactState;
